FROM pytorch/pytorch:1.8.1-cuda11.1-cudnn8-runtime

WORKDIR /data/cjcollazo/bsp_experiment_cyclic

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY unet.py ./
COPY ami_utility.py ./
COPY active_learning.py ./
COPY quickjson.py ./
COPY cv_folds.json ./
COPY config_bsp_cyclic_lowlr_iter_1.json ./
COPY config_histeq_cyclic_lowlr_iter_1.json ./
COPY config_histmatch_cyclic_lowlr_iter_1.json ./
COPY config_raw_cyclic_lowlr_iter_1.json ./
COPY active_learning_cyclic_lowlr.sh ./

CMD [ "sh", "active_learning_cyclic_lowlr.sh" ]