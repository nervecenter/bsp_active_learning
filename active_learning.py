"""
active_learning.py by Chris Collazo
Trains an active learning ensemble on AMI sections, including given active
sections if provided.
Tests an active learning ensemble on specified AMI sections.
Uses trained active learning ensemble to predict maps for a specified
active set.
Uses active learning ensemble predictions to vote on plurality ensemble maps,
calculate confidence levels.
Prints metrics for the active learning run, then gathers high confidence
samples within a sub-directory in the run's directory.
"""

import sys
import os
import gc

import numpy as np
import imageio as io
import pandas as pd
import tifffile as tif

from skimage.exposure import equalize_hist, match_histograms

import unet
import quickjson as qj
import ami_utility as amiu


# Load the training and validation set names
NUM_GPUS = int(sys.argv[-2])
CONFIG = qj.load_file(sys.argv[-1])

if NUM_GPUS > 1:
    FOLDS = qj.load_file("cv_folds.json")
    MAX_EPOCHS = 110
    SNAPSHOT_EPOCHS = list(range(10, MAX_EPOCHS + 1, 10))
    BATCH_SIZE = 32
    DATA_DIR = CONFIG["data_dir"]
    VERSIONS = [
        ("all_epochs", SNAPSHOT_EPOCHS),
        ("last3_epochs", SNAPSHOT_EPOCHS[-3:])
    ]
else:
    FOLDS = qj.load_file("test_cv_folds.json")
    MAX_EPOCHS = 1
    SNAPSHOT_EPOCHS = [1]
    BATCH_SIZE = 1
    DATA_DIR = "/home/chris/Data/ami"
    VERSIONS = [
        ("all_epochs", SNAPSHOT_EPOCHS)
    ]

RUN_NAME = CONFIG["run_name"]
PRE_PROC_TYPE = CONFIG["pre_proc_type"]
ACTIVE_SECTIONS = CONFIG["active_sections"]
CONF_THRESHOLD = CONFIG["conf_threshold"]




#
#
# TRAINING
#
#

print(f"Training for active learning run {RUN_NAME}...")

OPTIMIZER_CONFIG = CONFIG["optimizer_config"]
SCHEDULER_CONFIG = CONFIG["scheduler_config"] if "scheduler_config" in CONFIG else None

if not os.path.isdir(RUN_NAME):
    os.mkdir(RUN_NAME)

unet.seed_everything(2020)


for fold in ['a', 'b', 'c', 'd']:

    FOLD_DIR = f"{RUN_NAME}/fold_{fold}"
    SNAPSHOTS_DIR = f"{FOLD_DIR}/snapshots"

    if not os.path.isdir(FOLD_DIR):
        os.mkdir(FOLD_DIR)
    if not os.path.isdir(SNAPSHOTS_DIR):
        os.mkdir(SNAPSHOTS_DIR)

    if np.all([os.path.isfile(f"{SNAPSHOTS_DIR}/snapshot_epoch_{e}.pt") for e in SNAPSHOT_EPOCHS]):
        print(f"Already trained for run {RUN_NAME} fold {fold}, skipping...")
        continue

    print(f"Active training for run {RUN_NAME} fold {fold}")

    unet.train_ami(
        unet.make_gpu_parallel(unet.UnetSegmenter(
            num_input_channels=2,
            num_output_channels=3
        ), NUM_GPUS),
        epochs=MAX_EPOCHS,
        batch_size=BATCH_SIZE,
        optimizer_config=OPTIMIZER_CONFIG,
        scheduler_config=SCHEDULER_CONFIG,
        training_sections=FOLDS[fold]["training_sections"],
        validation_sections=FOLDS[fold]["validation_sections"],
        data_dir=DATA_DIR,
        accepted_sections=CONFIG["accepted_sections"],
        accepted_data_subdir=CONFIG["accepted_data_subdir"],
        channels_list=[1, 2],
        affix_list=[f"_{CONFIG['pre_proc_type']}"] * 2,
        metrics_file=f"{FOLD_DIR}/metrics_training.csv",
        snapshots_dir=SNAPSHOTS_DIR,
        checkpoints_list=SNAPSHOT_EPOCHS,
        use_gpu=NUM_GPUS > 0
    )

    print(f"Done training for active learning run {RUN_NAME} fold {fold}.")

print(f"Done training for active learning run {RUN_NAME}.")



#
#
# TESTING
#
#

print(f"\nTesting for active learning run {RUN_NAME}...")

for version_name, epoch_selection in VERSIONS:

    for fold in ['a', 'b', 'c', 'd']:

        print(f"Testing for active learning run {RUN_NAME} fold {fold} version {version_name}...\n")

        FOLD_DIR = f"{RUN_NAME}/fold_{fold}"
        SNAPSHOTS_DIR = f"{FOLD_DIR}/snapshots"
        VERSION_DIR = f"{FOLD_DIR}/{version_name}"
        TEST_DIR = f"{VERSION_DIR}/test"

        if not os.path.isdir(FOLD_DIR):
            os.mkdir(FOLD_DIR)
        if not os.path.isdir(VERSION_DIR):
            os.mkdir(VERSION_DIR)
        if not os.path.exists(TEST_DIR):
            os.mkdir(TEST_DIR)

        test_sections = FOLDS[fold]["test_sections"]

        if os.path.isfile(f"{TEST_DIR}/metrics_testing.csv") \
        and set(pd.read_csv(f"{TEST_DIR}/metrics_testing.csv")["section_name"].tolist()) == set(test_sections):
            print(f"Fold {fold} already tested, continuing...")
            continue


        # If it's a bsp run, perform BSP on the test section(s)
        if PRE_PROC_TYPE == "bsp":

            # p177 section brightness targets
            blue_target = [6.96745360309041, 20.28935167969]
            green_target = [0.968171754124953, 14.5505361922275]

            # Using epoch 100 snapshot to adjust section images
            last_epoch_snapshot = unet.UnetSegmenter(
                num_input_channels=2,
                num_output_channels=3,
                snapshot_file=f"{SNAPSHOTS_DIR}/snapshot_epoch_{epoch_selection[-1]}.pt"
            ).eval().cuda()

            for section_name in test_sections:
                blue, green, pred_softmax, _, _ = amiu.iterative_adjust_section(
                    DATA_DIR,
                    section_name,
                    blue_target,
                    green_target,
                    last_epoch_snapshot
                )

                # Save adjusted images
                io.imsave(f"{TEST_DIR}/{section_name}_1_bsp.png", blue)
                io.imsave(f"{TEST_DIR}/{section_name}_2_bsp.png", green)
                np.savez_compressed(
                    f"{TEST_DIR}/pred_softmax_{section_name}_epoch_{epoch_selection[-1]}.npz",
                    pred_softmax
                )

            del last_epoch_snapshot
            gc.collect()


        for epoch in epoch_selection:

            if PRE_PROC_TYPE == "bsp" and epoch == epoch_selection[-1]:
                print("Already produced BSP test adjustment, skipping last epoch...")
                continue

            print(f"Ensemble epoch {epoch}...")

            snapshot = unet.UnetSegmenter(
                num_input_channels=2,
                num_output_channels=3,
                snapshot_file=f"{SNAPSHOTS_DIR}/snapshot_epoch_{epoch}.pt"
            ).eval().cuda()

            for section_name in test_sections:

                print(f"\nProcessing active section {section_name}...")

                # If it's a raw, histeq, or histmatch run, load the existing Cohort 1 image
                if PRE_PROC_TYPE in ("raw", "histeq", "histmatch"):
                    prediction_softmax, prediction_map, _ = snapshot.predict_section(np.array([
                        io.imread(f"{DATA_DIR}/{section_name}_1_{PRE_PROC_TYPE}.png"),
                        io.imread(f"{DATA_DIR}/{section_name}_2_{PRE_PROC_TYPE}.png")
                    ]))
                elif PRE_PROC_TYPE == "bsp":
                    prediction_softmax, prediction_map, _ = snapshot.predict_section(np.array([
                        io.imread(f"{TEST_DIR}/{section_name}_1_{PRE_PROC_TYPE}.png"),
                        io.imread(f"{TEST_DIR}/{section_name}_2_{PRE_PROC_TYPE}.png")
                    ]))
                else:
                    print(f"Invalid preprocessing type: {PRE_PROC_TYPE}")
                    sys.exit()

                np.savez_compressed(
                    f"{TEST_DIR}/pred_softmax_{section_name}_epoch_{epoch}.npz",
                    prediction_softmax
                )

            del snapshot
            gc.collect()


        # Have snapshots vote on segmentation, calculate accuracy and confidence
        test_metrics = []

        for section_name in test_sections:

            # Load softmax maps
            epoch_softmaxes = np.array([
                np.load(
                    f"{TEST_DIR}/pred_softmax_{section_name}_epoch_{epoch}.npz"
                )["arr_0"]
                for epoch in epoch_selection
            ])

            # Average them
            ensemble_softmax = np.mean(epoch_softmaxes, axis=0)

            # Note highest confidence class as pixel confidence
            # Extract average confidence
            avg_confidence = float(np.mean(np.max(ensemble_softmax, axis=0)))
            assert 0 <= avg_confidence <= 1

            # Pick the highest confidence class, make class map
            ensemble_map = np.argmax(ensemble_softmax, axis=0)
            ensemble_image = amiu.ground_truth_map_to_image(ensemble_map)

            # Save ensemble softmax and image, and append metrics
            print(f"Saving predictions for test section {section_name}...")

            np.savez_compressed(f"{TEST_DIR}/pred_softmax_{section_name}.npz", ensemble_softmax)
            io.imsave(f"{TEST_DIR}/pred_{section_name}.png", ensemble_image)

            gt = io.imread(f"{DATA_DIR}/{section_name}_ground_truth_2ch.png")
            dice_score = unet.dice_coefficient(ensemble_image, gt)

            test_metrics.append([section_name, dice_score, avg_confidence])
            pd.DataFrame(
                test_metrics, columns=["section_name", "dice_score", "avg_confidence"]
            ).to_csv(f"{TEST_DIR}/metrics_testing.csv")

            del epoch_softmaxes, ensemble_softmax, ensemble_map, ensemble_image
            gc.collect()


        print(
            "Done testing for active learning for run ",
            f"{RUN_NAME} fold {fold} version {version_name}."
        )

print(f"Done testing for active learning run {RUN_NAME}.")



#
#
# PREDICTING
#
#


print(f"\nPredicting for active learning run {RUN_NAME}...")


# p177 section brightness targets
blue_target = [6.96745360309041, 20.28935167969]
green_target = [0.968171754124953, 14.5505361922275]


for version_name, epoch_selection in VERSIONS:

    # Pick the worst of the four folds and predict using that
    folds_compare = []

    for fold in ['a', 'b', 'c', 'd']:

        FOLD_DIR = f"{RUN_NAME}/fold_{fold}"
        VERSION_DIR = f"{FOLD_DIR}/{version_name}"
        TEST_DIR = f"{VERSION_DIR}/test"

        metrics = pd.read_csv(f"{TEST_DIR}/metrics_testing.csv")

        dice_conf = metrics["dice_score"].mean() * metrics["avg_confidence"].mean()
        folds_compare.append((fold, dice_conf))

    folds_compare.sort(key=lambda f: f[1])
    worst_fold, _ = folds_compare[0]

    print(f"Worst fold for run {RUN_NAME} version {version_name} was {worst_fold}...\n")

    VERSION_DIR = f"{RUN_NAME}/{version_name}"
    PREDICTIONS_DIR = f"{VERSION_DIR}/predictions"
    MAPS_DIR = f"{PREDICTIONS_DIR}/maps"
    ADJUSTED_DIR = f"{PREDICTIONS_DIR}/adjusted"

    if not os.path.exists(VERSION_DIR):
        os.mkdir(VERSION_DIR)
    if not os.path.exists(PREDICTIONS_DIR):
        os.mkdir(PREDICTIONS_DIR)
    if not os.path.exists(MAPS_DIR):
        os.mkdir(MAPS_DIR)
    if not os.path.exists(ADJUSTED_DIR):
        os.mkdir(ADJUSTED_DIR)

    with open(f"{VERSION_DIR}/worst_fold.txt", "w") as fp:
        fp.write(worst_fold)

    print(
        f"Predicting for active learning run {RUN_NAME}, ",
        f"version {version_name}, worst fold {worst_fold}...\n"
    )

    SNAPSHOTS_DIR = f"{RUN_NAME}/fold_{worst_fold}/snapshots"

    # For preprocessing methods, adjust all the active section images
    if PRE_PROC_TYPE == "bsp":

        last_epoch_snapshot = unet.UnetSegmenter(
            num_input_channels=2,
            num_output_channels=3,
            snapshot_file=f"{SNAPSHOTS_DIR}/snapshot_epoch_{epoch_selection[-1]}.pt"
        ).eval().cuda()

        # Using epoch 100 snapshot to adjust section images
        for section_name in ACTIVE_SECTIONS:

            adjusted_blue_file = f"{ADJUSTED_DIR}/{section_name}_1_bsp.png"
            adjusted_green_file = f"{ADJUSTED_DIR}/{section_name}_2_bsp.png"
            last_epoch_pred_file = f"{MAPS_DIR}/pred_softmax_{section_name}_epoch_{epoch_selection[-1]}.npz"

            if os.path.isfile(adjusted_blue_file) and os.path.isfile(adjusted_green_file) \
                and os.path.isfile(last_epoch_pred_file):

                print(f"Section {section_name} already adjusted, continuing...")
                continue

            adjusted_blue, adjusted_green, adjusted_softmax, _, _ = amiu.iterative_adjust_section(
                DATA_DIR,
                section_name,
                blue_target,
                green_target,
                last_epoch_snapshot
            )

            # Save adjusted images
            io.imsave(adjusted_blue_file, adjusted_blue)
            io.imsave(adjusted_green_file, adjusted_green)
            np.savez_compressed(last_epoch_pred_file, adjusted_softmax)

            del adjusted_blue, adjusted_green, adjusted_softmax
            gc.collect()

        del last_epoch_snapshot
        gc.collect()

    elif PRE_PROC_TYPE in ("histeq", "histmatch"):

        if PRE_PROC_TYPE == "histmatch":
            p177_blue = io.imread(f"{DATA_DIR}/p177_1_raw.png")
            p177_green = io.imread(f"{DATA_DIR}/p177_2_raw.png")

        # Adjust each section with histogram matching
        for section_name in ACTIVE_SECTIONS:

            adjusted_blue_file = f"{ADJUSTED_DIR}/{section_name}_1_{PRE_PROC_TYPE}.png"
            adjusted_green_file = f"{ADJUSTED_DIR}/{section_name}_2_{PRE_PROC_TYPE}.png"

            if os.path.isfile(adjusted_blue_file) and os.path.isfile(adjusted_green_file):
                print(f"Section {section_name} already adjusted, continuing...")
                continue

            section_blue = io.imread(f"{DATA_DIR}/{section_name}_1_raw.png")
            section_green = io.imread(f"{DATA_DIR}/{section_name}_2_raw.png")

            if PRE_PROC_TYPE == "histeq":
                adjusted_blue = (equalize_hist(section_blue) * 255).astype(np.uint8)
                adjusted_green = (equalize_hist(section_green) * 255).astype(np.uint8)
            else:
                adjusted_blue = match_histograms(section_blue, p177_blue).astype(np.uint8)
                adjusted_green = match_histograms(section_green, p177_green).astype(np.uint8)

            io.imsave(adjusted_blue_file, adjusted_blue)
            io.imsave(adjusted_green_file, adjusted_green)

            del section_blue, section_green, adjusted_blue, adjusted_green
            gc.collect()


    # Run predictions for the entire ensemble
    for epoch in epoch_selection:

        if PRE_PROC_TYPE == "bsp" and epoch == epoch_selection[-1]:
            print("Skipping final epoch for BSP, already did that beforehand...")

        snapshot = unet.UnetSegmenter(
            num_input_channels=2,
            num_output_channels=3,
            snapshot_file=f"{SNAPSHOTS_DIR}/snapshot_epoch_{epoch}.pt"
        ).eval().cuda()

        # For each section picked from 2- and 3-channel sets
        for section_name in ACTIVE_SECTIONS:

            print(f"Predicting epoch {epoch} for section {section_name}...")

            prediction_file = f"{MAPS_DIR}/pred_softmax_{section_name}_epoch_{epoch}.npz"

            if os.path.isfile(prediction_file):
                print(f"Epoch {epoch} for section {section_name} already predicted, continuing...")
                continue

            if PRE_PROC_TYPE == "raw":
                section_blue = io.imread(f"{DATA_DIR}/{section_name}_1_raw.png")
                section_green = io.imread(f"{DATA_DIR}/{section_name}_2_raw.png")
            else:
                section_blue = io.imread(f"{ADJUSTED_DIR}/{section_name}_1_{PRE_PROC_TYPE}.png")
                section_green = io.imread(f"{ADJUSTED_DIR}/{section_name}_2_{PRE_PROC_TYPE}.png")

            prediction_softmax, _, _ = snapshot.predict_section(np.array([
                section_blue, section_green
            ]))

            np.savez_compressed(prediction_file, prediction_softmax)

            del section_blue, section_green, prediction_softmax
            gc.collect()

        del snapshot
        gc.collect()


print(f"\nDone predicting for active learning run {RUN_NAME}.")



#
#
# CONFIDENCE
#
#


print(f"Computing confidences for run {RUN_NAME}...")


for version_name, epoch_selection in VERSIONS:

    VERSION_DIR = f"{RUN_NAME}/{version_name}"
    PREDICTIONS_DIR = f"{VERSION_DIR}/predictions"
    MAPS_DIR = f"{PREDICTIONS_DIR}/maps"

    assert os.path.exists(VERSION_DIR)
    assert os.path.exists(PREDICTIONS_DIR)
    assert os.path.exists(MAPS_DIR)

    confidences_file = f"{PREDICTIONS_DIR}/confidences_{RUN_NAME}_{version_name}.json"

    confidences = {} if not os.path.isfile(confidences_file) \
        else qj.load_file(confidences_file)

    for section_name in ACTIVE_SECTIONS:
        for epoch in epoch_selection:
            pred_map_file = f"{MAPS_DIR}/pred_softmax_{section_name}_epoch_{epoch}.npz"
            assert os.path.isfile(pred_map_file), f"{pred_map_file} doesn't exist!"

    for section_name in ACTIVE_SECTIONS:

        if os.path.isfile(f"{PREDICTIONS_DIR}/pred_{section_name}.png") \
        and section_name in confidences:
            print(f"Confidence already calculated for section {section_name}, skipping...")
            continue

        # Load softmax maps
        epoch_softmaxes = np.array([
            np.load(
                f"{MAPS_DIR}/pred_softmax_{section_name}_epoch_{epoch}.npz"
            )["arr_0"]
            for epoch in epoch_selection
        ])

        # Average all epoch softmaxes for ensemble softmax
        ensemble_softmax = np.mean(epoch_softmaxes, axis=0)

        # Max of softmax at each pixel is pixel confidence
        # Mean of pixel confidences is average section confidence
        avg_confidence = float(np.mean(np.max(ensemble_softmax, axis=0)))
        assert 0 <= avg_confidence <= 1

        # Store the section's confidence, save with each loop
        confidences[section_name] = avg_confidence
        qj.save_file(confidences_file, confidences)

        # Pick the highest confidence class, make class map image, save it
        print(f"Saving predictions for active section {section_name}...")

        io.imsave(
            f"{PREDICTIONS_DIR}/pred_{section_name}.png",
            amiu.ground_truth_map_to_image(np.argmax(ensemble_softmax, axis=0))
        )

        del epoch_softmaxes, ensemble_softmax
        gc.collect()


print(f"Done computing confidences for run {RUN_NAME}.")



#
#
# WRAP UP
#
#

print(f"Wrapping up run {RUN_NAME}...")

for version_name, _ in VERSIONS:

    VERSION_DIR = f"{RUN_NAME}/{version_name}"
    PREDICTIONS_DIR = f"{VERSION_DIR}/predictions"
    MAPS_DIR = f"{PREDICTIONS_DIR}/maps"
    ADJUSTED_DIR = f"{PREDICTIONS_DIR}/adjusted"
    HIGH_CONF_DIR = f"{VERSION_DIR}/high_conf"

    if not os.path.exists(HIGH_CONF_DIR):
        os.mkdir(HIGH_CONF_DIR)

    with open(f"{VERSION_DIR}/worst_fold.txt", "r") as fp:
        worst_fold = fp.read().strip()

    test_dice = [
        np.mean(pd.read_csv(
            f"{RUN_NAME}/fold_{fold}/{version_name}/test/metrics_testing.csv"
        )["dice_score"].tolist())
        for fold in ['a', 'b', 'c', 'd']
    ]
    # test_conf = [
    #     np.mean(pd.read_csv(
    #         f"{RUN_NAME}/fold_{fold}/{version_name}/test/metrics_testing.csv"
    #     )["avg_confidence"].tolist())
    #     for fold in ['a', 'b', 'c', 'd']
    # ]

    # Read the confidences json
    confidences = qj.load_file(f"{PREDICTIONS_DIR}/confidences_{RUN_NAME}_{version_name}.json")
    med_conf_sections = [s for s in confidences if confidences[s] >= 0.9]
    high_conf_sections = [s for s in confidences if confidences[s] >= CONF_THRESHOLD]

     # Print out run results
    print(
        f"\nResults for active learning run {RUN_NAME},\n",
        f"worst fold {worst_fold}, preprocessing type {PRE_PROC_TYPE},\n",
        f"version {version_name}:\n"
    )
    for fold, dice in zip(['a', 'b', 'c', 'd'], test_dice):
        print(f"Fold {('(worst) ' if fold == worst_fold else '') + fold} test Dice: {dice:0.4f}")
    print(f"Average test Dice: {np.mean(test_dice):0.4f}")
    # for fold, conf in zip(['a', 'b', 'c', 'd'], test_conf):
    #     print(f"Fold {('(worst) ' if fold == worst_fold else '') + fold} test confidence: {conf:0.4f}")
    # print(f"Average test confidence: {np.mean(test_conf):0.4f}")
    print(f"# sections over 90% confidence threshold: {len(med_conf_sections)}/{len(confidences)}")
    print(f"# sections over 97% confidence threshold: {len(high_conf_sections)}/{len(confidences)}\n")

    qj.save_file(
        f"{HIGH_CONF_DIR}/expert_review.json",
        {s : "waiting_for_review" for s in high_conf_sections}
    )
    qj.save_file(
        f"{HIGH_CONF_DIR}/confidences_{RUN_NAME}_{version_name}.json",
        confidences
    )

    if PRE_PROC_TYPE != "raw":
        for section_name in high_conf_sections:

            print(f"Saving for section {section_name} with confidence {confidences[section_name]:0.4f}...")

            blue_adjusted = io.imread(f"{ADJUSTED_DIR}/{section_name}_1_{PRE_PROC_TYPE}.png")
            green_adjusted = io.imread(f"{ADJUSTED_DIR}/{section_name}_2_{PRE_PROC_TYPE}.png")
            ground_truth = io.imread(f"{PREDICTIONS_DIR}/pred_{section_name}.png")

            height, width = ground_truth.shape[:2]

            blue = np.dstack((blue_adjusted, blue_adjusted, blue_adjusted))
            green = np.dstack((green_adjusted, green_adjusted, green_adjusted))

            tif.imwrite(f"{HIGH_CONF_DIR}/{section_name}.tif", blue, compress=6, append=True)
            tif.imwrite(f"{HIGH_CONF_DIR}/{section_name}.tif", green, compress=6, append=True)
            tif.imwrite(f"{HIGH_CONF_DIR}/{section_name}.tif", ground_truth, compress=6, append=True)


print(f"Done wrapping up run {RUN_NAME}.")
