"""
ami_utility.py by Chris Collazo
Helper functions for handling fluorescence images of sections for
acute myocardial infarction deep learning and active learning.
"""

from math import isclose

import numpy as np
import imageio as io
from skimage.exposure import adjust_gamma
from skimage.filters import threshold_otsu

# Disable decompression bomb warnings for large images
from warnings import simplefilter
from PIL.Image import DecompressionBombWarning
simplefilter('ignore', DecompressionBombWarning)


# original_sections = [
#     "p177", "p179", "p270", "p271", "p272", "p273",
#     "p274", "p277", "p300", "p301", "p302"
# ]

def ground_truth_map_to_image(pred_map):

    assert pred_map.ndim == 2, f"Expected 2-dimensional prediction map, got {pred_map.ndim} dimensions."

    height, width = pred_map.shape[:2]

    prediction_image = np.full((height, width, 4), (255, 0, 255, 255), dtype=np.uint8)
    prediction_image[pred_map == 0] = (0, 0, 0, 0)
    prediction_image[pred_map == 1] = (255, 255, 0, 255)
    prediction_image[pred_map == 2] = (0, 255, 0, 255)

    none = np.all(prediction_image == (0, 0, 0, 0), axis=-1)
    normal = np.all(prediction_image == (0, 255, 0, 255), axis=-1)
    risk = np.all(prediction_image == (255, 255, 0, 255), axis=-1)
    assert np.all(none | normal | risk), f"Prediction map has an invalid class color, can't turn to image."

    return prediction_image


def ground_truth_image_to_1hot(gt_image):

    border_map = np.all(gt_image == (255,255,0,255), axis=-1)
    normal_map = np.all(gt_image == (0,255,0,255), axis=-1)
    none_map = np.all(gt_image == (0,0,0,0), axis=-1)
    assert np.all(none_map == ~(border_map | normal_map))

    gt_1hot = (np.array((none_map, border_map, normal_map)) * 1).astype(np.uint8)
    assert gt_1hot.dtype == np.uint8
    assert np.max(gt_1hot) == 1

    return gt_1hot


def ground_truth_image_to_map(gt_image):

    border_map = np.all(gt_image == (255,255,0,255), axis=-1)
    normal_map = np.all(gt_image == (0,255,0,255), axis=-1)
    none_map = np.all(gt_image == (0,0,0,0), axis=-1)
    assert np.all(none_map == ~(border_map | normal_map))

    gt_map = np.zeros(gt_image.shape[:2], dtype=np.uint8)
    gt_map[none_map] = 0
    gt_map[border_map] = 1
    gt_map[normal_map] = 2
    assert gt_map.dtype == np.uint8
    assert np.all((gt_map == 0) | (gt_map == 1) | (gt_map == 2))

    return gt_map


def tile_locations(height, width):
    """
    Return a list of tuples of the (j, i) coordinates of every tile location
    for the given image size.
    """
    tile_height = height // 512
    tile_width = width // 512

    y_coords = [j * 512 for j in range(tile_height)]
    x_coords = [i * 512 for i in range(tile_width)]
    tile_locs = [(j, i) for j in y_coords for i in x_coords]

    return tile_locs


def tile_locations_evaluation(height, width):
    """
    Return a list of tuples of the (j, i) coordinates of every tile location
    for the given image size, with smaller shifts for evaluation
    of a whole-slide image.
    """
    tile_width = (width - 256) // 256
    tile_height = (height - 256) // 256

    y_coords = [j * 256 for j in range(tile_height)]
    x_coords = [i * 256 for i in range(tile_width)]
    tile_locs = [(j, i) for j in y_coords for i in x_coords]

    # Add on the right and bottom sides for processing
    tile_locs += [(j, width - 512) for j in y_coords]
    tile_locs += [(height - 512, i) for i in x_coords]

    return tile_locs


def load_samples(data_dir, section, channels_list, affix_list):

    print(f"Loading samples for section {section}...")

    num_channels = len(channels_list)
    assert len(affix_list) == num_channels

    section_raw = np.array([
        io.imread(f"{data_dir}/{section}_{channel}{affix}.png")
        for channel, affix in zip(channels_list, affix_list)
    ]).astype(np.float32) / 255.0
    assert section_raw.shape[0] == num_channels

    height, width = section_raw.shape[1:3]
    
    tile_locs = tile_locations(height, width)
    num_tiles = len(tile_locs)

    tiles = np.array([
        section_raw[:, j : j + 512, i : i + 512]
        for (j, i) in tile_locs
    ])
    assert tiles.shape == (num_tiles, num_channels, 512, 512), f"tiles shape: {tiles.shape}"

    return tiles


def load_ground_truth_samples_2ch(data_dir, section):

    print(f"Loading ground truth samples for section {section}...")

    section_gt = io.imread(f"{data_dir}/{section}_ground_truth_2ch.png")
    assert section_gt.shape[2] == 4

    height, width = section_gt.shape[:2]
    
    tile_locs = tile_locations(height, width)
    num_tiles = len(tile_locs)

    segmentation = ground_truth_image_to_map(section_gt)

    tiles_maps = np.array([
        segmentation[j : j + 512, i : i + 512]
        for (j, i) in tile_locs
    ])
    assert tiles_maps.shape == (num_tiles, 512, 512), f"tiles_maps shape: {tiles_maps.shape}"

    return tiles_maps


def evaluation_samples_from_raw(section_raw):

    print("Loading overlapped evaluation samples for raw input...")

    section_raw = section_raw.astype(np.float32) / 255.0
    num_channels = section_raw.shape[0]

    height, width = section_raw.shape[1:3]
    
    tile_locs = tile_locations_evaluation(height, width)
    num_tiles = len(tile_locs)

    tiles = np.array([
        section_raw[:, j : j + 512, i : i + 512]
        for (j, i) in tile_locs
    ])
    assert tiles.shape == (num_tiles, num_channels, 512, 512), f"tiles shape: {tiles.shape}"

    return tiles, tile_locs, height, width


def load_evaluation_samples(data_dir, section, channels_list, affix_list):
    
    print(f"Loading overlapped evaluation samples for section {section}...")

    num_channels = len(channels_list)
    assert len(affix_list) == num_channels

    section_raw = np.array([
        io.imread(f"{data_dir}/{section}_{channel}{affix}.png")
        for channel, affix in zip(channels_list, affix_list)
    ]).astype(np.float32) / 255.0
    assert section_raw.shape[0] == num_channels

    height, width = section_raw.shape[1:3]
    
    tile_locs = tile_locations_evaluation(height, width)
    num_tiles = len(tile_locs)

    tiles = np.array([
        section_raw[:, j : j + 512, i : i + 512]
        for (j, i) in tile_locs
    ])
    assert tiles.shape == (num_tiles, num_channels, 512, 512), f"tiles shape: {tiles.shape}"

    return tiles, tile_locs, height, width


def load_evaluation_ground_truth_samples_2ch(data_dir, section):
    
    print(f"Loading overlapped evaluation ground truth samples for section {section}...")

    section_gt = io.imread(f"{data_dir}/{section}_ground_truth_2ch.png")
    assert section_gt.shape[2] == 4

    height, width = section_gt.shape[:2]
    
    tile_locs = tile_locations_evaluation(height, width)
    num_tiles = len(tile_locs)

    segmentation = ground_truth_image_to_map(section_gt)

    tiles_maps = np.array([
        segmentation[j : j + 512, i : i + 512]
        for (j, i) in tile_locs
    ])
    assert tiles_maps.shape == (num_tiles, 512, 512), f"tiles_maps shape: {tiles_maps.shape}"

    return tiles_maps


def match_intensity_euclidean(image,
                              class_map,
                              target_intensities,
                              num_classes):

    def class_mean(image, class_map, class_num, baseline):
        vals = image[class_map == class_num]
        return np.mean(vals) if np.any(vals) else baseline

    assert num_classes >= np.max(class_map) + 1
    assert num_classes == len(target_intensities)

    target_intensities = np.array(target_intensities)
    intensity_log = {
        "target_intensities": target_intensities.tolist(),
        "adjustments": []
    }

    # Scale of gamma adjustment, grows until we hit 1.0
    delta_gamma = 0.05

    # Compute metrics on the initial input image
    # The average class intensities
    # The per-class distance
    # The relative Euclidean distance, positive or negative (brighter or darker)
    # And the distance magnitude
    prev_image = image
    print(f"Initial image for Euclidean adjustment: {prev_image}")
    prev_image_avg_intensities = np.array([
        class_mean(prev_image, class_map, c, target_intensities[c])
        for c in range(num_classes)
    ])
    prev_intensity_dists = target_intensities - prev_image_avg_intensities
    prev_dist_relative = np.sum(prev_intensity_dists)
    prev_dist_magnitude = np.linalg.norm(prev_intensity_dists)

    while True:

        # print(f"Distance magnitude: {prev_dist_magnitude}")

        # Depending on relative distance, lighten or darken
        # the PREVIOUS IMAGE to get closer
        if prev_dist_relative > 0:
            adjusted_image = adjust_gamma(prev_image, 1.0 - delta_gamma)
        else:
            adjusted_image = adjust_gamma(prev_image, 1.0 + delta_gamma)

        # Get adjusted image intensities, relative distance,
        # distance magnitude to target
        adjusted_image_avg_intensities = np.array([
            class_mean(adjusted_image, class_map, c, target_intensities[c])
            for c in range(num_classes)
        ])
        adjusted_intensity_dists = target_intensities - adjusted_image_avg_intensities
        adjusted_dist_relative = np.sum(adjusted_intensity_dists)
        adjusted_dist_magnitude = np.linalg.norm(adjusted_intensity_dists)

        # If the adjusted image is FURTHER than the previous or stops,
        # the prev image is the one we want, break to return it
        if adjusted_dist_magnitude > prev_dist_magnitude \
        or isclose(adjusted_dist_magnitude, prev_dist_magnitude):
            adjusted_image = prev_image
            break
        # Otherwise, the last adjustment is fine, keep going
        else:
            prev_image = adjusted_image
            prev_image_avg_intensities = adjusted_image_avg_intensities
            prev_intensity_dists = adjusted_intensity_dists
            prev_dist_relative = adjusted_dist_relative
            prev_dist_magnitude = adjusted_dist_magnitude

            intensity_log["adjustments"].append(
                prev_image_avg_intensities.tolist()
            )

    return adjusted_image, intensity_log


def reconstruct_evaluation(predicted_tiles_softmax,
                           tile_locs,
                           height,
                           width):

    num_classes = predicted_tiles_softmax.shape[1]
    prediction_softmax = np.zeros((num_classes, height, width), dtype=np.float32)

    for n, (j, i) in enumerate(tile_locs):

        # Slice out the center 256x256 of the tile for the final confidence map
        prediction_softmax[
            :,
            j + 128 : j + 384,
            i + 128 : i + 384
        ] = predicted_tiles_softmax[n, :, 128:384, 128:384]

        # Keep the left if this is the first column
        if i == 0:
            prediction_softmax[
                :,
                j : j + 512,
                i : i + 128
            ] = predicted_tiles_softmax[n, :, :, 0:128]

        # Keep the top if this is the first row
        if j == 0:
            prediction_softmax[
                :,
                j : j + 128,
                i : i + 512
            ] = predicted_tiles_softmax[n, :, 0:128, :]

        # Keep the right if this is the right side
        if i == width - 512:
            prediction_softmax[
                :,
                j : j + 512,
                i + 384 : i + 512
            ] = predicted_tiles_softmax[n, :, :, 384:512]

        # Keep the bottom if this is the bottom side
        if j == height - 512:
            prediction_softmax[
                :,
                j + 384 : j + 512,
                i : i + 512
            ] = predicted_tiles_softmax[n, :, 384:512, :]

    prediction_map = np.argmax(prediction_softmax, axis=0)
    prediction_image = ground_truth_map_to_image(prediction_map)

    return prediction_softmax, prediction_map, prediction_image


def iterative_adjust_section(data_dir,
                             section_name,
                             blue_target,
                             green_target,
                             model,
                             num_iterations=5):

    print(f"Iterative adjustment for section {section_name}...")

    blue_raw = io.imread(f"{data_dir}/{section_name}_1_raw.png")
    green_raw = io.imread(f"{data_dir}/{section_name}_2_raw.png")

    height, width = blue_raw.shape[:2]
    assert (height, width) == green_raw.shape[:2]

    # Otsu threshold, save as class map, then use to adjust for iter 1
    blue_otsu = blue_raw >= threshold_otsu(blue_raw)
    green_otsu = green_raw >= threshold_otsu(green_raw)

    prev_blue_map = blue_otsu
    prev_green_map = green_otsu
    

    #
    # ITERATE, adjusting brightness and generating new maps each time
    #

    for itr in range(1, num_iterations + 1):

        print(f"Iterative adjustment step {itr}")

        # print("Matching intensities...")
        blue_distmin, _ = match_intensity_euclidean(
            blue_raw, prev_blue_map * 1, blue_target, 2
        )
        green_distmin, _ = match_intensity_euclidean(
            green_raw, prev_green_map * 1, green_target, 2
        )

        # Evaluate first class map
        # print("Evaluating...")
        prediction_softmax, prediction_map, prediction_image = model.predict_section(np.array([
            blue_distmin, green_distmin
        ]))
        assert prediction_map.shape == (height, width)

        # print("Isolating classes...")
        prev_blue_map = ((prediction_map == 1) | (prediction_map == 2))
        prev_green_map = (prediction_map == 2)

    return blue_distmin, green_distmin, prediction_softmax, prediction_map, prediction_image


# Uncomment to test iterative adjustment using a model you've trained



def main():
    from unet import UnetSegmenter

    data_dir = "/data/cjcollazo/ami"
    section = "p274"
    blue_target = [6.96745360309041, 20.28935167969]
    green_target = [0.968171754124953, 14.5505361922275]

    from unet import UnetSegmenter
    model = UnetSegmenter(
        num_input_channels=2,
        num_output_channels=3,
        snapshot_file=f"bsp_cyclic_lowlr_iter_1/fold_a/snapshots/snapshot_epoch_110.pt"
    ).eval().cuda()

    # Test iterative adjustment
    blue, green, _, _, gt = iterative_adjust_section(
        data_dir,
        section,
        blue_target,
        green_target,
        model
    )

    io.imsave("blue_image.png", blue)
    io.imsave("green_image.png", green)
    io.imsave("ground_truth_2ch.png", gt)

if __name__ == "__main__":
    main()
