"""
unet.py by Chris Collazo
Defines a scaled-down Unet in PyTorch using PyTorch Lightning utilities for
convenience and automation.
"""

import random
import os

import numpy as np
import pandas as pd
import torch
from torch import nn
from torch.nn.functional import cross_entropy, softmax
from torch.utils.data import TensorDataset, DataLoader

import ami_utility


def dice_coefficient(prediction, target):
    assert prediction.ndim >= 2 and target.ndim >= 2
    height, width = prediction.shape[:2]
    assert target.shape[:2] == (height, width)

    if prediction.ndim == 3 and target.ndim == 3:
        correctness_map = np.all(
            prediction == target,
            axis=-1
        )
    elif prediction.ndim == 2 and target.ndim == 2:
        correctness_map = prediction == target
    else:
        raise ValueError("Inputs to dice_coefficient() should be images, one-hot maps, or class integer maps.")

    return float(np.count_nonzero(correctness_map) / (height * width))


def seed_everything(TORCH_SEED):
    random.seed(TORCH_SEED)
    os.environ['PYTHONHASHSEED'] = str(TORCH_SEED)
    np.random.seed(TORCH_SEED)
    torch.manual_seed(TORCH_SEED)
    torch.cuda.manual_seed_all(TORCH_SEED)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


def make_gpu_parallel(model, num_gpus):
    if num_gpus > 0 and torch.cuda.is_available():
        assert torch.cuda.device_count() >= num_gpus, \
            f"Tried to use {num_gpus} GPUs, found only {torch.cuda.device_count()}."
        device = torch.device("cuda:0")
        model = nn.DataParallel(model, device_ids=list(range(num_gpus)))
    else:
        device = torch.device("cpu")
    model.to(device)
    return model


class UnetSegmenter(nn.Module):

    def __init__(self,
                 num_input_channels=2,
                 num_output_channels=3,
                 snapshot_file=None):

        super(UnetSegmenter, self).__init__()

        self.num_input_channels = num_input_channels
        self.num_output_channels = num_output_channels
        self.snapshot_file = snapshot_file

        print("Hyperparameters:")
        print(f"Number of input channels: {self.num_input_channels}")
        print(f"Number of output channels: {self.num_output_channels}")
        if snapshot_file is not None:
            print(f"Loading from snaphot file: {self.snapshot_file}")

    
        self.maxpool = nn.MaxPool2d(2)

        self.down1 = nn.Sequential(
            nn.Conv2d(self.num_input_channels, 16, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(16, 16, 3, padding=1),
            nn.ReLU(inplace=True),
        )

        self.down2 = nn.Sequential(
            nn.Conv2d(16, 32, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(32, 32, 3, padding=1),
            nn.ReLU(inplace=True),
        )

        self.down3 = nn.Sequential(
            nn.Conv2d(32, 64, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 64, 3, padding=1),
            nn.ReLU(inplace=True),
        )

        self.down4 = nn.Sequential(
            nn.Conv2d(64, 128, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(128, 128, 3, padding=1),
            nn.ReLU(),
        )

        self.mid = nn.Sequential(
            nn.Conv2d(128, 256, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, 3, padding=1),
            nn.ReLU()
        )

        self.dropout = nn.Dropout(p=0.5, inplace=True)

        self.up1_1 = nn.Sequential(
            nn.Upsample(scale_factor=2, mode="bilinear", align_corners=True),
            nn.Conv2d(256, 128, 3, padding=1),
            nn.ReLU(inplace=True)
        )
        self.up1_2 = nn.Sequential(
            nn.Conv2d(256, 128, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(128, 128, 3, padding=1),
            nn.ReLU(inplace=True)
        )

        self.up2_1 = nn.Sequential(
            nn.Upsample(scale_factor=2, mode="bilinear", align_corners=True),
            nn.Conv2d(128, 64, 3, padding=1),
            nn.ReLU(inplace=True)
        )
        self.up2_2 = nn.Sequential(
            nn.Conv2d(128, 64, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 64, 3, padding=1),
            nn.ReLU(inplace=True)
        )

        self.up3_1 = nn.Sequential(
            nn.Upsample(scale_factor=2, mode="bilinear", align_corners=True),
            nn.Conv2d(64, 32, 3, padding=1),
            nn.ReLU(inplace=True)
        )
        self.up3_2 = nn.Sequential(
            nn.Conv2d(64, 32, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(32, 32, 3, padding=1),
            nn.ReLU(inplace=True)
        )

        self.up4_1 = nn.Sequential(
            nn.Upsample(scale_factor=2, mode="bilinear", align_corners=True),
            nn.Conv2d(32, 16, 3, padding=1),
            nn.ReLU(inplace=True)
        )
        self.up4_2 = nn.Sequential(
            nn.Conv2d(32, 16, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(16, 16, 3, padding=1),
            nn.ReLU(inplace=True)
        )

        self.finisher = nn.Sequential(
            nn.Conv2d(16, 8, 3, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(8, self.num_output_channels, 1)
            # nn.Softmax(dim=1)
        )

        if snapshot_file is not None:
            self.load_state_dict({
                (key.replace("module.", "") if "module." in key else key): val
                for key, val in torch.load(snapshot_file).items()
            })

       


    def forward(self, x):

        d1 = self.down1(x)
        mp1 = self.maxpool(d1)

        d2 = self.down2(mp1)
        mp2 = self.maxpool(d2)

        d3 = self.down3(mp2)
        mp3 = self.maxpool(d3)

        d4 = self.down4(mp3)
        do4 = self.dropout(d4)
        mp4 = self.maxpool(do4)

        m = self.mid(mp4)
        do_m = self.dropout(m)

        u1_1 = self.up1_1(do_m)
        mrg1 = torch.cat([do4, u1_1], dim=1)
        u1_2 = self.up1_2(mrg1)

        u2_1 = self.up2_1(u1_2)
        mrg2 = torch.cat([d3, u2_1], dim=1)
        u2_2 = self.up2_2(mrg2)

        u3_1 = self.up3_1(u2_2)
        mrg3 = torch.cat([d2, u3_1], dim=1)
        u3_2 = self.up3_2(mrg3)

        u4_1 = self.up4_1(u3_2)
        mrg4 = torch.cat([d1, u4_1], dim=1)
        u4_2 = self.up4_2(mrg4)

        fin = self.finisher(u4_2)

        return fin


    def is_on_cuda(self):
        return next(self.parameters()).is_cuda


    def predict_section(self, section_raw):
        tiles, tile_locs, height, width = \
            ami_utility.evaluation_samples_from_raw(section_raw)

        assert tiles.shape[1] == self.num_input_channels

        inputs = [
            torch.reshape(
                torch.tensor(tile).float(),
                (1, self.num_input_channels, 512, 512)
            ) for tile in tiles
        ]

        if self.is_on_cuda():
            inputs = [i.cuda() for i in inputs]

        tile_predictions_softmax = np.array([
            softmax(
                self(i), dim=1
            ).cpu().detach().numpy().reshape(
                self.num_output_channels, 512, 512
            ).astype(np.float32)
            for i in inputs
        ])

        pred_softmax, pred_map, pred_image = ami_utility.reconstruct_evaluation(
            tile_predictions_softmax, tile_locs, height, width
        )

        return pred_softmax, pred_map, pred_image


def train_ami(model,
              epochs=None,
              batch_size=None,
              optimizer_config=None,
              scheduler_config=None,
              training_sections=None,
              validation_sections=None,
              data_dir=None,
              accepted_sections=None,
              accepted_data_subdir=None,
              channels_list=None,
              affix_list=None,
              metrics_file=None,
              snapshots_dir=None,
              checkpoints_list=None,
              use_gpu=False):

    print("Loading training samples.")

    cohort1_inputs = [
        ami_utility.load_samples(
            data_dir, section, channels_list, affix_list
        ) for section in training_sections
    ]
    cohort1_ground_truths = [
        ami_utility.load_ground_truth_samples_2ch(data_dir, section)
        for section in training_sections
    ]

    accepted_inputs = [
        ami_utility.load_samples(
            f"{data_dir}/{accepted_data_subdir}", section, channels_list, affix_list
        ) for section in accepted_sections
    ]
    accepted_ground_truths = [
        ami_utility.load_ground_truth_samples_2ch(f"{data_dir}/{accepted_data_subdir}", section)
        for section in accepted_sections
    ]

    training_inputs = np.concatenate(
        cohort1_inputs + accepted_inputs, axis=0
    )
    training_ground_truths = np.concatenate(
        cohort1_ground_truths + accepted_ground_truths, axis=0
    )
    assert training_inputs.shape[0] == training_ground_truths.shape[0]

    print(f"Number of training samples: {training_inputs.shape[0]}")

    train_loader = DataLoader(
        TensorDataset(
            torch.Tensor(training_inputs).float(),
            torch.Tensor(training_ground_truths).long()
        ),
        batch_size=batch_size,
        num_workers=4,
        pin_memory=True,
        shuffle=True
        # transform=transforms.ToTensor()
    )

    print("Loading validation samples.")

    validation_inputs = np.concatenate([
        ami_utility.load_samples(data_dir, section, channels_list, affix_list)
        for section in validation_sections
    ], axis=0)

    validation_ground_truths = np.concatenate([
        ami_utility.load_ground_truth_samples_2ch(data_dir, section)
        for section in validation_sections
    ], axis=0)
    assert validation_inputs.shape[0] == validation_ground_truths.shape[0]

    print(f"Number of validation samples: {validation_inputs.shape[0]}")

    valid_loader = DataLoader(
        TensorDataset(
            torch.Tensor(validation_inputs).float(),
            torch.Tensor(validation_ground_truths).long()
        ),
        batch_size=batch_size,
        num_workers=4,
        pin_memory=True
        # transform=transforms.ToTensor()
    )

    # BSP Cyclic
    if optimizer_config["algorithm"] == "SGD":
        optimizer = torch.optim.SGD(
            model.parameters(),
            lr=optimizer_config["lr"],
            momentum=optimizer_config["momentum"],
            weight_decay=optimizer_config["weight_decay"],
            dampening=optimizer_config["dampening"],
            nesterov=optimizer_config["nesterov"]
        )
    elif optimizer_config["algorithm"] == "Adam":
        optimizer = torch.optim.Adam(
            model.parameters(),
            lr=optimizer_config["lr"],
            betas=optimizer_config["betas"]
        )
    elif optimizer_config["algorithm"] == "Adadelta":
        optimizer = torch.optim.Adadelta(
            model.parameters(),
            lr=optimizer_config["lr"],      # PyTorch default: 1.0, Keras default: 0.001
            rho=optimizer_config["rho"],    # PyTorch default: 0.9, Keras default: 0.95
            eps=optimizer_config["eps"]     # PyTorch default: 1e-6, Keras default: 1e-7
        )
    else:
        raise ValueError(f"Invalid optimizer configuration: {optimizer_config}")

    scheduler = None
    if scheduler_config is not None:
        if scheduler_config["algorithm"] == "CosineAnnealingWarmRestarts":
            scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
                optimizer,
                T_0=scheduler_config["T_0"] * len(train_loader)
            )


    metrics = []
    print("Beginning training...")

    for e in range(1, epochs + 1):

        print(f"Epoch {e}...")

        if scheduler is not None:
            learning_rate = scheduler.get_last_lr()[0]

        train_losses = []
        model.train()

        for data, labels in train_loader:

            if use_gpu:
                data, labels = data.cuda(), labels.cuda()
              
            optimizer.zero_grad()

            pred = model(data)

            loss = cross_entropy(pred,labels)
            train_losses.append(loss.item())
            loss.backward()

            nn.utils.clip_grad_value_(model.parameters(), clip_value=1.0)
            optimizer.step()

            if scheduler is not None:
                scheduler.step()

        train_loss = np.mean(train_losses)


        valid_losses = []
        model.eval()

        for data, labels in valid_loader:

            if use_gpu:
                data, labels = data.cuda(), labels.cuda()
              
            pred = model(data)
            
            loss = cross_entropy(pred, labels)
            valid_losses.append(loss.item())

        valid_loss = np.mean(valid_losses)


        # print(f"Epoch {e}: {train_loss=}, {valid_loss=}, {learning_rate=}")
        # print(f"Epoch {e}: {train_loss=:0.4f}, {valid_loss=:0.4f}, {learning_rate=:0.4f}")
        # print(f"Epoch {e}: train_loss={train_loss:0.4f}, valid_loss={valid_loss:0.4f}, learning_rate={learning_rate:0.4f}")

        if scheduler is not None:
            print(f"Epoch {e}: train_loss={train_loss:0.4f}, valid_loss={valid_loss:0.4f}, learning_rate={learning_rate:0.4f}")
            metrics.append([e, train_loss, valid_loss, learning_rate])
            pd.DataFrame(
                metrics, columns=["epoch", "train_loss", "valid_loss", "learning_rate"]
            ).to_csv(metrics_file, index=False)
        else:
            print(f"Epoch {e}: train_loss={train_loss:0.4f}, valid_loss={valid_loss:0.4f}")
            metrics.append([e, train_loss, valid_loss])
            pd.DataFrame(
                metrics, columns=["epoch", "train_loss", "valid_loss"]
            ).to_csv(metrics_file, index=False)

        if e in checkpoints_list:
            print(f"Saving at checkpoint epoch {e}...")
            torch.save(model.state_dict(), f"{snapshots_dir}/snapshot_epoch_{e}.pt")
            # torch.save({
            #     "epoch": e,
            #     "model_state_dict": model.state_dict(),
            #     "optimizer_state_dict": optimizer.state_dict(),
            #     "scheduler_state_dict": scheduler.state_dict(),
            #     "loss": train_loss,
            # }, f"{snapshots_dir}/snapshot_epoch_{e}.pt")


    print("Finished training.")
    return model


def main():
    
    # Debug model architecture
    model = UnetSegmenter(num_input_channels=2, num_output_channels=3)
    model.cuda()
    model.summarize(mode="full")

    # from torchsummary import summary
    # summary(model.cuda(), (2, 512, 512))


if __name__ == "__main__":
    main()
