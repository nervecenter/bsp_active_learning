#! /bin/bash
#$ -cwd

# export CUDA_VISIBLE_DEVICES=0,1,2,3
# export CUDA_HOME=/apps/cuda/cuda-10.2/
# export CUDA_PATH=/apps/cuda/cuda-10.2/
# export PATH=$PATH:/apps/cuda/cuda-10.2/bin
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/apps/cuda/cuda-10.2/lib64

optimizer="cyclic_lowlr"

for config in config*$optimizer*.json; do
        echo $config
        python3 active_learning.py 4 $config
done